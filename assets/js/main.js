$(document).ready(function() {
   $(window).scroll(function() {
    if($(this).scrollTop() >=5){
       $('nav').addClass('sticky');
    } else {
       $('nav').removeClass('sticky');
    }
   })
   $("#btn-menu").click(function() {
    $('.menu').toggleClass("open-menu");
   });
   $('.menu li:has(ul)').click(function(e) {
       e.preventDefault();
       if($(this).hasClass('activated')) {
           $(this).removeClass('activated');
           $(this).children('ul').slideUp();
       } else {
           $('.menu li ul').slideUp();
           $('.menu li').removeClass('activated');
           $(this).addClass('activated');
           $(this).children('ul').slideDown();
       }
   });
   $('.menu li ul a').click(function() {
       window.location.href = $(this).attr('href');
   });
});